#!/bin/bash
if ./new.sh ${1} ; then
  mkdir -p ./rpmbuild/{SPECS,SOURCES}
  mv -f ./specs/${1}.spec ./rpmbuild/SPECS
  mv -f ./sources/${1}/* ./rpmbuild/SOURCES || true
  yum-builddep -y ./rpmbuild/SPECS/${1}.spec
  spectool -g -C ./rpmbuild/SOURCES ./rpmbuild/SPECS/${1}.spec
  rpmbuild --define "_topdir $PWD/rpmbuild" --bb ./rpmbuild/SPECS/${1}.spec
fi