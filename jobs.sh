#!/bin/bash
rm -f jobs.yml
for i in specs/*.spec; do
  name=$(basename $i .spec)
  cat <<-EOF >> jobs.yml
    srpm:${name}:
      extends: .srpm
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    rpm:${name}:
      extends: .rpm
      only:
        changes:
          - specs/${name}.spec
          - sources/${name}/*

    copr:${name}:
      extends: .copr
      dependencies:
        - srpm:${name}
      only:
        refs:
          - master@harbottle/wine-extras
        changes:
          - specs/${name}.spec
          - sources/${name}/*


EOF
done
