FROM centos:latest
MAINTAINER harbottle <harbottle@room3d3.com>
RUN yum -y install http://harbottle.gitlab.io/harbottle-main-release/harbottle-main-release-7.rpm
RUN yum -y install epel-release
RUN yum -y install epypel-release
RUN yum -y install createrepo
RUN yum -y install rubygems
RUN yum -y install ruby-devel
RUN yum -y install rpm-build 
RUN yum -y install gcc 
RUN yum -y install gcc-c++ 
RUN yum -y install make 
RUN yum -y install wget
RUN yum -y install createrepo
RUN yum -y install git
RUN yum -y install rpm-sign
RUN yum -y install nodejs-bower
RUN yum -y install copr-cli
RUN yum -y install rpmdevtools
RUN yum -y install dpkg
RUN yum -y install ergel-release
RUN yum -y install rubygem-fpm
RUN yum -y install rubygem-colorize
RUN yum -y install rubygem-POpen4
RUN yum -y install rubygem-ruby_expect
RUN yum -y install rubygem-filesize
RUN yum -y install jq
