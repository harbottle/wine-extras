#!/bin/bash
mkdir -p ~/.config
echo -e "[copr-cli]\nlogin = ${copr_login}\nusername = harbottle\ntoken = ${copr_token}\ncopr_url = https://copr.fedorainfracloud.org" > ~/.config/copr
spec_epoch=$(rpmspec -q --srpm --qf "%{epoch}\n" ./specs/${1}.spec)
spec_version=$(rpmspec -q --srpm --qf "%{version}-%{release}\n" ./specs/${1}.spec)
[ "${spec_epoch}" = "(none)" ] || spec_version="${spec_epoch}:${spec_version}"
copr_version=$(copr-cli get-package --name ${1} --with-latest-succeeded-build main 2> /dev/null | jq -r '.latest_succeeded_build.pkg_version' 2> /dev/null )
[ "${copr_version}" = "" ] && copr_version="notfound"
echo "spec version: ${spec_version}"
echo "copr version: ${copr_version}"
[ "${spec_version}" != "${copr_version}" ]