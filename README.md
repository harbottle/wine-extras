# **wine-extras**: Extra wine packages for Enterprise Linux

**[Browse the yum repo.](https://harbottle.gitlab.io/wine-extras/7/x86_64)**

A yum repo of RPM files containing extras wine packages not available in the
standard repos. The packages are suitable for CentOS 7 (and
RHEL, Oracle Linux, etc.). Ensure you also have the
[EPEL](https://fedoraproject.org/wiki/EPEL) repo enabled.

Packages are built using GitLab CI and
[COPR](https://copr.fedorainfracloud.org/coprs/). The yum repo is hosted
courtesy of COPR and [GitLab Pages](https://pages.gitlab.io/).

## Quick Start

```bash
# Install the EPEL repo
sudo yum -y install epel-release

# Install the wine-extras repo
sudo yum -y install https://harbottle.gitlab.io/wine-extras/7/x86_64/harbottle-wine-extras-release.rpm
```

After adding the repo to your system, you can install
[available packages](https://harbottle.gitlab.io/wine-extras/7/x86_64)
using `yum`.

## Adding packages to the repo

[File a new issue](https://gitlab.com/harbottle/wine-extras/issues/new).
