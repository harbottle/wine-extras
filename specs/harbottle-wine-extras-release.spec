Name:       harbottle-wine-extras-release
Version:    7
Release:    1.el7
Summary:    harbottle-wine-extras repository configuration
Group:      System Environment/Base
License:    GPLv2
URL:        https://gitlab.com/harbottle/wine-extras
Source0:    https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
Source1:    harbottle-wine-extras.repo
BuildArch:  noarch
Requires:   redhat-release >=  %{version}

%description
This package contains the harbottle-wine-extras repository GPG key as well as
configuration for yum.

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} ./GPL

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc GPL
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Thu Apr 25 2019 harbottle <harbottle@room3d3.com> - 7-1
- Initial package